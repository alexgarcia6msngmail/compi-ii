﻿namespace Compi
{
    partial class MainFrame
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslResultadoSintactico = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNuevo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbrirArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGuardarComo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGuardarTodo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.cerrarPestañaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarProyectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.herramientasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAnalisisLexico = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAnalisisSintactico = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMacros = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvErrores = new System.Windows.Forms.DataGridView();
            this.error = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorlinea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvTablaSimbolos = new System.Windows.Forms.DataGridView();
            this.columntkn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lexema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcPestañas = new System.Windows.Forms.TabControl();
            this.gbTablaSimbolos = new System.Windows.Forms.GroupBox();
            this.gbManejadorErrores = new System.Windows.Forms.GroupBox();
            this.txtArchivo = new System.Windows.Forms.TextBox();
            this.ofdArchivo = new System.Windows.Forms.OpenFileDialog();
            this.sfdArchivo = new System.Windows.Forms.SaveFileDialog();
            this.btnAnalisisInteligente = new System.Windows.Forms.Button();
            this.lblRenglon1 = new System.Windows.Forms.Label();
            this.lblColumna1 = new System.Windows.Forms.Label();
            this.gbListaPolish = new System.Windows.Forms.GroupBox();
            this.dgvListaPolish = new System.Windows.Forms.DataGridView();
            this.clPolish = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRenglon = new System.Windows.Forms.TextBox();
            this.txtColumna = new System.Windows.Forms.TextBox();
            this.dgvListaPosfijo = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvListaPosfijo = new System.Windows.Forms.GroupBox();
            this.dgvListaCompleta = new System.Windows.Forms.DataGridView();
            this.listacompletaElementos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbListaCompleta = new System.Windows.Forms.GroupBox();
            this.gbListaVariables = new System.Windows.Forms.GroupBox();
            this.dgvListaVariables = new System.Windows.Forms.DataGridView();
            this.lvestado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lvSimbolo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivContenido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnMacro = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaSimbolos)).BeginInit();
            this.gbTablaSimbolos.SuspendLayout();
            this.gbManejadorErrores.SuspendLayout();
            this.gbListaPolish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPolish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPosfijo)).BeginInit();
            this.gvListaPosfijo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaCompleta)).BeginInit();
            this.gbListaCompleta.SuspendLayout();
            this.gbListaVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaVariables)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1,
            this.tsslResultadoSintactico});
            this.statusStrip1.Location = new System.Drawing.Point(0, 634);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1228, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(111, 17);
            this.ToolStripStatusLabel1.Text = "Analisis Sintáctico : ";
            // 
            // tsslResultadoSintactico
            // 
            this.tsslResultadoSintactico.Name = "tsslResultadoSintactico";
            this.tsslResultadoSintactico.Size = new System.Drawing.Size(25, 17);
            this.tsslResultadoSintactico.Text = " ___";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.herramientasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1228, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNuevo,
            this.tsmiAbrirArchivo,
            this.toolStripMenuItem1,
            this.tsmiGuardar,
            this.tsmiGuardarComo,
            this.tsmiGuardarTodo,
            this.toolStripMenuItem2,
            this.cerrarPestañaToolStripMenuItem,
            this.cerrarProyectoToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // tsmiNuevo
            // 
            this.tsmiNuevo.Name = "tsmiNuevo";
            this.tsmiNuevo.Size = new System.Drawing.Size(156, 22);
            this.tsmiNuevo.Text = "Nuevo";
            this.tsmiNuevo.Click += new System.EventHandler(this.tsmiNuevo_Click);
            // 
            // tsmiAbrirArchivo
            // 
            this.tsmiAbrirArchivo.Name = "tsmiAbrirArchivo";
            this.tsmiAbrirArchivo.Size = new System.Drawing.Size(156, 22);
            this.tsmiAbrirArchivo.Text = "Abrir Archivo";
            this.tsmiAbrirArchivo.Click += new System.EventHandler(this.tsmiAbrirArchivo_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 6);
            // 
            // tsmiGuardar
            // 
            this.tsmiGuardar.Name = "tsmiGuardar";
            this.tsmiGuardar.Size = new System.Drawing.Size(156, 22);
            this.tsmiGuardar.Text = "Guardar";
            this.tsmiGuardar.Click += new System.EventHandler(this.tsmiGuardar_Click);
            // 
            // tsmiGuardarComo
            // 
            this.tsmiGuardarComo.Name = "tsmiGuardarComo";
            this.tsmiGuardarComo.Size = new System.Drawing.Size(156, 22);
            this.tsmiGuardarComo.Text = "Guardar Como";
            this.tsmiGuardarComo.Click += new System.EventHandler(this.tsmiGuardarComo_Click);
            // 
            // tsmiGuardarTodo
            // 
            this.tsmiGuardarTodo.Name = "tsmiGuardarTodo";
            this.tsmiGuardarTodo.Size = new System.Drawing.Size(156, 22);
            this.tsmiGuardarTodo.Text = "Guardar Todo";
            this.tsmiGuardarTodo.Click += new System.EventHandler(this.tsmiGuardarTodo_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(153, 6);
            // 
            // cerrarPestañaToolStripMenuItem
            // 
            this.cerrarPestañaToolStripMenuItem.Name = "cerrarPestañaToolStripMenuItem";
            this.cerrarPestañaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.cerrarPestañaToolStripMenuItem.Text = "Cerrar Pestaña";
            // 
            // cerrarProyectoToolStripMenuItem
            // 
            this.cerrarProyectoToolStripMenuItem.Name = "cerrarProyectoToolStripMenuItem";
            this.cerrarProyectoToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.cerrarProyectoToolStripMenuItem.Text = "Cerrar Proyecto";
            // 
            // herramientasToolStripMenuItem
            // 
            this.herramientasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAnalisisLexico,
            this.tsmiAnalisisSintactico,
            this.tsmiMacros});
            this.herramientasToolStripMenuItem.Name = "herramientasToolStripMenuItem";
            this.herramientasToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.herramientasToolStripMenuItem.Text = "Herramientas";
            // 
            // tsmiAnalisisLexico
            // 
            this.tsmiAnalisisLexico.Name = "tsmiAnalisisLexico";
            this.tsmiAnalisisLexico.Size = new System.Drawing.Size(169, 22);
            this.tsmiAnalisisLexico.Text = "Análisis Léxico";
            this.tsmiAnalisisLexico.Click += new System.EventHandler(this.tsmiAnalisisLexico_Click);
            // 
            // tsmiAnalisisSintactico
            // 
            this.tsmiAnalisisSintactico.Name = "tsmiAnalisisSintactico";
            this.tsmiAnalisisSintactico.Size = new System.Drawing.Size(169, 22);
            this.tsmiAnalisisSintactico.Text = "Análisis Sintáctico";
            this.tsmiAnalisisSintactico.Click += new System.EventHandler(this.tsmiAnalisisSintactico_Click);
            // 
            // tsmiMacros
            // 
            this.tsmiMacros.Name = "tsmiMacros";
            this.tsmiMacros.Size = new System.Drawing.Size(169, 22);
            this.tsmiMacros.Text = "Macros";
            this.tsmiMacros.Click += new System.EventHandler(this.tsmiMacros_Click);
            // 
            // dgvErrores
            // 
            this.dgvErrores.AllowUserToDeleteRows = false;
            this.dgvErrores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvErrores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.error,
            this.descripcion,
            this.errorlinea});
            this.dgvErrores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvErrores.Location = new System.Drawing.Point(3, 16);
            this.dgvErrores.Name = "dgvErrores";
            this.dgvErrores.RowHeadersVisible = false;
            this.dgvErrores.Size = new System.Drawing.Size(425, 153);
            this.dgvErrores.TabIndex = 5;
            // 
            // error
            // 
            this.error.HeaderText = "Error";
            this.error.Name = "error";
            this.error.Width = 60;
            // 
            // descripcion
            // 
            this.descripcion.HeaderText = "Descripcion";
            this.descripcion.Name = "descripcion";
            this.descripcion.Width = 300;
            // 
            // errorlinea
            // 
            this.errorlinea.HeaderText = "Linea";
            this.errorlinea.Name = "errorlinea";
            this.errorlinea.Width = 60;
            // 
            // dgvTablaSimbolos
            // 
            this.dgvTablaSimbolos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTablaSimbolos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columntkn,
            this.estado,
            this.lexema,
            this.linea});
            this.dgvTablaSimbolos.Location = new System.Drawing.Point(10, 19);
            this.dgvTablaSimbolos.Name = "dgvTablaSimbolos";
            this.dgvTablaSimbolos.RowHeadersVisible = false;
            this.dgvTablaSimbolos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTablaSimbolos.Size = new System.Drawing.Size(194, 401);
            this.dgvTablaSimbolos.TabIndex = 6;
            // 
            // columntkn
            // 
            this.columntkn.HeaderText = "Token";
            this.columntkn.Name = "columntkn";
            this.columntkn.Width = 50;
            // 
            // estado
            // 
            this.estado.HeaderText = "Edo.";
            this.estado.Name = "estado";
            this.estado.Width = 40;
            // 
            // lexema
            // 
            this.lexema.HeaderText = "Lexema";
            this.lexema.Name = "lexema";
            this.lexema.Width = 50;
            // 
            // linea
            // 
            this.linea.HeaderText = "Linea";
            this.linea.Name = "linea";
            this.linea.Width = 50;
            // 
            // tbcPestañas
            // 
            this.tbcPestañas.Location = new System.Drawing.Point(0, 27);
            this.tbcPestañas.Name = "tbcPestañas";
            this.tbcPestañas.SelectedIndex = 0;
            this.tbcPestañas.Size = new System.Drawing.Size(590, 538);
            this.tbcPestañas.TabIndex = 7;
            this.tbcPestañas.Selected += new System.Windows.Forms.TabControlEventHandler(this.tbcPestañas_Selected);
            this.tbcPestañas.Deselected += new System.Windows.Forms.TabControlEventHandler(this.tbcPestañas_Deselected);
            // 
            // gbTablaSimbolos
            // 
            this.gbTablaSimbolos.Controls.Add(this.dgvTablaSimbolos);
            this.gbTablaSimbolos.Location = new System.Drawing.Point(586, 27);
            this.gbTablaSimbolos.Name = "gbTablaSimbolos";
            this.gbTablaSimbolos.Size = new System.Drawing.Size(209, 426);
            this.gbTablaSimbolos.TabIndex = 8;
            this.gbTablaSimbolos.TabStop = false;
            this.gbTablaSimbolos.Text = "Tabla de Símbolos";
            // 
            // gbManejadorErrores
            // 
            this.gbManejadorErrores.Controls.Add(this.dgvErrores);
            this.gbManejadorErrores.Location = new System.Drawing.Point(589, 459);
            this.gbManejadorErrores.Name = "gbManejadorErrores";
            this.gbManejadorErrores.Size = new System.Drawing.Size(431, 172);
            this.gbManejadorErrores.TabIndex = 0;
            this.gbManejadorErrores.TabStop = false;
            this.gbManejadorErrores.Text = "Manejador de Errores";
            // 
            // txtArchivo
            // 
            this.txtArchivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArchivo.Location = new System.Drawing.Point(0, 52);
            this.txtArchivo.Multiline = true;
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Size = new System.Drawing.Size(580, 506);
            this.txtArchivo.TabIndex = 9;
            this.txtArchivo.TextChanged += new System.EventHandler(this.txtArchivo_TextChanged);
            // 
            // ofdArchivo
            // 
            this.ofdArchivo.Filter = "Archivos de Texto (.txt)|*.txt";
            this.ofdArchivo.InitialDirectory = "c:\\";
            // 
            // sfdArchivo
            // 
            this.sfdArchivo.Filter = "Archivos de Texto (.txt)|*.txt";
            this.sfdArchivo.InitialDirectory = "c:\\";
            // 
            // btnAnalisisInteligente
            // 
            this.btnAnalisisInteligente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnalisisInteligente.Location = new System.Drawing.Point(175, 572);
            this.btnAnalisisInteligente.Name = "btnAnalisisInteligente";
            this.btnAnalisisInteligente.Size = new System.Drawing.Size(267, 51);
            this.btnAnalisisInteligente.TabIndex = 10;
            this.btnAnalisisInteligente.Text = "Análisis Inteligente";
            this.btnAnalisisInteligente.UseVisualStyleBackColor = true;
            this.btnAnalisisInteligente.Click += new System.EventHandler(this.btnAnalisisInteligente_Click);
            // 
            // lblRenglon1
            // 
            this.lblRenglon1.AutoSize = true;
            this.lblRenglon1.Location = new System.Drawing.Point(471, 575);
            this.lblRenglon1.Name = "lblRenglon1";
            this.lblRenglon1.Size = new System.Drawing.Size(47, 13);
            this.lblRenglon1.TabIndex = 14;
            this.lblRenglon1.Text = "Renglon";
            // 
            // lblColumna1
            // 
            this.lblColumna1.AutoSize = true;
            this.lblColumna1.Location = new System.Drawing.Point(471, 601);
            this.lblColumna1.Name = "lblColumna1";
            this.lblColumna1.Size = new System.Drawing.Size(48, 13);
            this.lblColumna1.TabIndex = 15;
            this.lblColumna1.Text = "Columna";
            // 
            // gbListaPolish
            // 
            this.gbListaPolish.Controls.Add(this.dgvListaPolish);
            this.gbListaPolish.Location = new System.Drawing.Point(1026, 289);
            this.gbListaPolish.Name = "gbListaPolish";
            this.gbListaPolish.Size = new System.Drawing.Size(69, 334);
            this.gbListaPolish.TabIndex = 17;
            this.gbListaPolish.TabStop = false;
            this.gbListaPolish.Text = "Polish";
            // 
            // dgvListaPolish
            // 
            this.dgvListaPolish.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaPolish.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clPolish});
            this.dgvListaPolish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaPolish.Location = new System.Drawing.Point(3, 16);
            this.dgvListaPolish.Name = "dgvListaPolish";
            this.dgvListaPolish.RowHeadersVisible = false;
            this.dgvListaPolish.Size = new System.Drawing.Size(63, 315);
            this.dgvListaPolish.TabIndex = 18;
            // 
            // clPolish
            // 
            this.clPolish.HeaderText = "Elementos";
            this.clPolish.Name = "clPolish";
            this.clPolish.Width = 60;
            // 
            // txtRenglon
            // 
            this.txtRenglon.Location = new System.Drawing.Point(524, 572);
            this.txtRenglon.Name = "txtRenglon";
            this.txtRenglon.ReadOnly = true;
            this.txtRenglon.Size = new System.Drawing.Size(24, 20);
            this.txtRenglon.TabIndex = 18;
            this.txtRenglon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtColumna
            // 
            this.txtColumna.Location = new System.Drawing.Point(524, 598);
            this.txtColumna.Name = "txtColumna";
            this.txtColumna.ReadOnly = true;
            this.txtColumna.Size = new System.Drawing.Size(24, 20);
            this.txtColumna.TabIndex = 19;
            this.txtColumna.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgvListaPosfijo
            // 
            this.dgvListaPosfijo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaPosfijo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgvListaPosfijo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaPosfijo.Location = new System.Drawing.Point(3, 16);
            this.dgvListaPosfijo.Name = "dgvListaPosfijo";
            this.dgvListaPosfijo.RowHeadersVisible = false;
            this.dgvListaPosfijo.Size = new System.Drawing.Size(63, 236);
            this.dgvListaPosfijo.TabIndex = 20;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Elementos";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 60;
            // 
            // gvListaPosfijo
            // 
            this.gvListaPosfijo.Controls.Add(this.dgvListaPosfijo);
            this.gvListaPosfijo.Location = new System.Drawing.Point(1029, 27);
            this.gvListaPosfijo.Name = "gvListaPosfijo";
            this.gvListaPosfijo.Size = new System.Drawing.Size(69, 255);
            this.gvListaPosfijo.TabIndex = 21;
            this.gvListaPosfijo.TabStop = false;
            this.gvListaPosfijo.Text = "Posfijo";
            // 
            // dgvListaCompleta
            // 
            this.dgvListaCompleta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaCompleta.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.listacompletaElementos});
            this.dgvListaCompleta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaCompleta.Location = new System.Drawing.Point(3, 16);
            this.dgvListaCompleta.Name = "dgvListaCompleta";
            this.dgvListaCompleta.RowHeadersVisible = false;
            this.dgvListaCompleta.Size = new System.Drawing.Size(118, 576);
            this.dgvListaCompleta.TabIndex = 22;
            // 
            // listacompletaElementos
            // 
            this.listacompletaElementos.HeaderText = "Elementos";
            this.listacompletaElementos.Name = "listacompletaElementos";
            this.listacompletaElementos.Width = 120;
            // 
            // gbListaCompleta
            // 
            this.gbListaCompleta.Controls.Add(this.dgvListaCompleta);
            this.gbListaCompleta.Location = new System.Drawing.Point(1104, 27);
            this.gbListaCompleta.Name = "gbListaCompleta";
            this.gbListaCompleta.Size = new System.Drawing.Size(124, 595);
            this.gbListaCompleta.TabIndex = 23;
            this.gbListaCompleta.TabStop = false;
            this.gbListaCompleta.Text = "Completa";
            // 
            // gbListaVariables
            // 
            this.gbListaVariables.Controls.Add(this.dgvListaVariables);
            this.gbListaVariables.Location = new System.Drawing.Point(801, 27);
            this.gbListaVariables.Name = "gbListaVariables";
            this.gbListaVariables.Size = new System.Drawing.Size(219, 426);
            this.gbListaVariables.TabIndex = 24;
            this.gbListaVariables.TabStop = false;
            this.gbListaVariables.Text = "Lista de variables";
            // 
            // dgvListaVariables
            // 
            this.dgvListaVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaVariables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lvestado,
            this.lvSimbolo,
            this.ivNombre,
            this.ivContenido});
            this.dgvListaVariables.Location = new System.Drawing.Point(10, 19);
            this.dgvListaVariables.Name = "dgvListaVariables";
            this.dgvListaVariables.RowHeadersVisible = false;
            this.dgvListaVariables.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvListaVariables.Size = new System.Drawing.Size(203, 401);
            this.dgvListaVariables.TabIndex = 0;
            // 
            // lvestado
            // 
            this.lvestado.Frozen = true;
            this.lvestado.HeaderText = "Edo.";
            this.lvestado.Name = "lvestado";
            this.lvestado.Width = 40;
            // 
            // lvSimbolo
            // 
            this.lvSimbolo.HeaderText = "Simbolo";
            this.lvSimbolo.Name = "lvSimbolo";
            this.lvSimbolo.Width = 50;
            // 
            // ivNombre
            // 
            this.ivNombre.HeaderText = "Nombre";
            this.ivNombre.Name = "ivNombre";
            this.ivNombre.Width = 50;
            // 
            // ivContenido
            // 
            this.ivContenido.HeaderText = "Contenido";
            this.ivContenido.Name = "ivContenido";
            this.ivContenido.Width = 60;
            // 
            // btnMacro
            // 
            this.btnMacro.Location = new System.Drawing.Point(52, 572);
            this.btnMacro.Name = "btnMacro";
            this.btnMacro.Size = new System.Drawing.Size(105, 50);
            this.btnMacro.TabIndex = 25;
            this.btnMacro.Text = "Macro";
            this.btnMacro.UseVisualStyleBackColor = true;
            this.btnMacro.Click += new System.EventHandler(this.btnMacro_Click);
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 656);
            this.Controls.Add(this.btnMacro);
            this.Controls.Add(this.gbListaVariables);
            this.Controls.Add(this.gvListaPosfijo);
            this.Controls.Add(this.gbListaCompleta);
            this.Controls.Add(this.txtColumna);
            this.Controls.Add(this.txtRenglon);
            this.Controls.Add(this.gbListaPolish);
            this.Controls.Add(this.lblColumna1);
            this.Controls.Add(this.lblRenglon1);
            this.Controls.Add(this.btnAnalisisInteligente);
            this.Controls.Add(this.txtArchivo);
            this.Controls.Add(this.gbManejadorErrores);
            this.Controls.Add(this.gbTablaSimbolos);
            this.Controls.Add(this.tbcPestañas);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainFrame";
            this.Text = "Compiña! :) Hecho por Alejandro García Carballo";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvErrores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTablaSimbolos)).EndInit();
            this.gbTablaSimbolos.ResumeLayout(false);
            this.gbManejadorErrores.ResumeLayout(false);
            this.gbListaPolish.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPolish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaPosfijo)).EndInit();
            this.gvListaPosfijo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaCompleta)).EndInit();
            this.gbListaCompleta.ResumeLayout(false);
            this.gbListaVariables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaVariables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem herramientasToolStripMenuItem;
        private System.Windows.Forms.DataGridView dgvErrores;
        private System.Windows.Forms.TabControl tbcPestañas;
        private System.Windows.Forms.GroupBox gbTablaSimbolos;
        private System.Windows.Forms.GroupBox gbManejadorErrores;
        private System.Windows.Forms.ToolStripStatusLabel tsslResultadoSintactico;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbrirArchivo;
        private System.Windows.Forms.ToolStripMenuItem tsmiGuardarComo;
        private System.Windows.Forms.TextBox txtArchivo;
        private System.Windows.Forms.OpenFileDialog ofdArchivo;
        private System.Windows.Forms.SaveFileDialog sfdArchivo;
        private System.Windows.Forms.ToolStripMenuItem cerrarPestañaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarProyectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiGuardar;
        private System.Windows.Forms.ToolStripMenuItem tsmiNuevo;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsmiGuardarTodo;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsmiAnalisisLexico;
        private System.Windows.Forms.ToolStripMenuItem tsmiAnalisisSintactico;
        private System.Windows.Forms.Button btnAnalisisInteligente;
        private System.Windows.Forms.Label lblRenglon1;
        private System.Windows.Forms.Label lblColumna1;
        private System.Windows.Forms.GroupBox gbListaPolish;
        private System.Windows.Forms.DataGridView dgvListaPolish;
        private System.Windows.Forms.TextBox txtRenglon;
        private System.Windows.Forms.TextBox txtColumna;
        private System.Windows.Forms.DataGridView dgvListaPosfijo;
        private System.Windows.Forms.GroupBox gvListaPosfijo;
        private System.Windows.Forms.DataGridView dgvListaCompleta;
        private System.Windows.Forms.GroupBox gbListaCompleta;
        private System.Windows.Forms.DataGridView dgvTablaSimbolos;
        private System.Windows.Forms.DataGridViewTextBoxColumn columntkn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn lexema;
        private System.Windows.Forms.DataGridViewTextBoxColumn linea;
        private System.Windows.Forms.DataGridViewTextBoxColumn clPolish;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.GroupBox gbListaVariables;
        private System.Windows.Forms.DataGridView dgvListaVariables;
        private System.Windows.Forms.DataGridViewTextBoxColumn lvestado;
        private System.Windows.Forms.DataGridViewTextBoxColumn lvSimbolo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivContenido;
        private System.Windows.Forms.DataGridViewTextBoxColumn error;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn errorlinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn listacompletaElementos;
        private System.Windows.Forms.ToolStripMenuItem tsmiMacros;
        private System.Windows.Forms.Button btnMacro;
    }
}

