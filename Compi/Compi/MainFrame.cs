﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Compi.Recursos.Clases.Interfaz;
using Compi.Recursos.Clases.Analizadores;
using System.Collections;

namespace Compi
{
    public partial class MainFrame : Form
    {
        Lexico AnalizadorLexico = new Lexico();
        Sintactico AnalizadorSintactico = new Sintactico();
        List<Token> ListaTokens = new List<Token>();

        List<Plantilla> ListaPlantillas = new List<Plantilla>();
        Plantilla plantilla;
        LogicaInterfaz ObjLogica = new LogicaInterfaz();
        MetodosIO ObjIO = new MetodosIO();

        bool AnalisisInteligente = false;
        public MainFrame()
        {
            InitializeComponent();
        }

        private void tsmiAbrirArchivo_Click(object sender, EventArgs e)
        {
            if (ofdArchivo.ShowDialog() == DialogResult.OK)
            {
                plantilla = ObjIO.AbrirArchivo(ListaPlantillas, ofdArchivo.FileName);
                if (plantilla == null)
                {
                    return;
                }

                //Agregar la plantilla recien creada
                ListaPlantillas.Add(plantilla);
                tbcPestañas.TabPages.Add(plantilla.Cabecera);
                tbcPestañas.SelectedIndex = tbcPestañas.TabPages.Count - 1;
                txtArchivo.Text = plantilla.Contenido;
                
            }
        }

        private void tsmiGuardarComo_Click(object sender, EventArgs e)
        {
            if (sfdArchivo.ShowDialog() ==  DialogResult.OK)
            {
                File.WriteAllText(sfdArchivo.FileName, txtArchivo.Text);
            }
        }
        private void tsmiGuardar_Click(object sender, EventArgs e)
        {
            if (ListaPlantillas.Count > 0)
            {
                ListaPlantillas[tbcPestañas.SelectedIndex].Contenido = txtArchivo.Text;
                File.WriteAllText(ListaPlantillas[tbcPestañas.SelectedIndex].Ruta, txtArchivo.Text);
            }
        }

        private void tsmiNuevo_Click(object sender, EventArgs e)
        {
            if (sfdArchivo.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfdArchivo.FileName, "");

                plantilla = ObjIO.AbrirArchivo(ListaPlantillas, sfdArchivo.FileName);
                if (plantilla == null)
                {
                    return;
                }
                ListaPlantillas.Add(plantilla);
                tbcPestañas.TabPages.Add(plantilla.Cabecera);
                tbcPestañas.SelectedIndex = tbcPestañas.TabPages.Count - 1;

                txtArchivo.Text = plantilla.Contenido;

            }
        }
        private void tbcPestañas_Deselected(object sender, TabControlEventArgs e)
        {
            tsmiGuardar_Click(sender, e);
        }
        private void tbcPestañas_Selected(object sender, TabControlEventArgs e)
        { 
            txtArchivo.Text = ObjLogica.ActualizarTextbox(ListaPlantillas, tbcPestañas.SelectedTab.Text);
        }

        private void tsmiGuardarTodo_Click(object sender, EventArgs e)
        {
            
            for (int i = 0; i < ListaPlantillas.Count ; i++)
            {
                if (ListaPlantillas[i].Cabecera == tbcPestañas.SelectedTab.Text)
                {
                    ListaPlantillas[i].Contenido = txtArchivo.Text;
                }
                File.WriteAllText(ListaPlantillas[i].Ruta, ListaPlantillas[i].Contenido);
            }
            
        }

        private void tsmiAnalisisLexico_Click(object sender, EventArgs e)
        {
            dgvErrores.DataSource = null;
            dgvErrores.Rows.Clear();

            dgvTablaSimbolos.DataSource = null;
            dgvTablaSimbolos.Rows.Clear();

            dgvListaVariables.DataSource = null;
            dgvListaVariables.Rows.Clear();

            dgvListaPolish.DataSource = null;
            dgvListaPolish.Rows.Clear();

            dgvListaPosfijo.DataSource = null;
            dgvListaPosfijo.Rows.Clear();

            dgvListaCompleta.DataSource = null;
            dgvListaCompleta.Rows.Clear();

            AnalizadorLexico.Ejecutar(txtArchivo.Text+ (char)165);
            
            ListaTokens.Clear();
            foreach (var Token in AnalizadorLexico.ListaTokens)
            {
                if (Token.estado > -500)
                {
                    ListaTokens.Add(Token);
                    dgvTablaSimbolos.Rows.Add
                        (dgvTablaSimbolos.RowCount,Token.estado, Token.lexema, Token.linea);
                }
                else
                {
                    dgvErrores.Rows.Add(Token.estado, Token.lexema, Token.linea);
                }
            }
            
        }

        private void tsmiAnalisisSintactico_Click(object sender, EventArgs e)
        {
            tsmiAnalisisLexico_Click(this, e);
            if (AnalizadorLexico.ListaTokens.Count>0)
            {
                tsslResultadoSintactico.Text = AnalizadorSintactico.Ejecutar(ListaTokens);

                txtRenglon.Text = Convert.ToString(AnalizadorSintactico.Renglon);
                txtColumna.Text = Convert.ToString(AnalizadorSintactico.Columna);


                foreach (var Variable in AnalizadorSintactico.Variables)
                {
                    dgvListaVariables.Rows.Add(
                        Variable.estado,
                        Variable.simbolo,
                        Variable.nombre,
                        Variable.contenido
                        );
                }
                foreach (var Error in AnalizadorSintactico.Errores)
                {
                    dgvErrores.Rows.Add(
                        Error.token,
                        Error.mensaje, 
                        Error.linea
                        );
                }
                foreach (var Elemento in AnalizadorSintactico.ListaPolish)
                {
                    dgvListaPolish.Rows.Add(
                        Convert.ToString(Elemento.lexema)
                        );
                }
                foreach (var Elemento in AnalizadorSintactico.ListaPosfijo)
                {
                    dgvListaPosfijo.Rows.Add(
                        Convert.ToString(Elemento.lexema)
                        );
                }
                foreach (var Elemento in AnalizadorSintactico.PolishCompleto)
                {
                    dgvListaCompleta.Rows.Add(
                        Convert.ToString(Elemento.lexema)
                        );
                }
            }
        }

        private void txtArchivo_TextChanged(object sender, EventArgs e)
        {
            if (AnalisisInteligente)
            {
                tsmiAnalisisSintactico_Click(this, e);
            }
        }

        private void btnAnalisisInteligente_Click(object sender, EventArgs e)
        {
            if (AnalisisInteligente==false)
            {
                AnalisisInteligente = true;
                btnAnalisisInteligente.BackColor = Color.LawnGreen;
                txtArchivo_TextChanged(this, e);
            }
            else
            {
                AnalisisInteligente = false;
                tsslResultadoSintactico.Text = "___";
                btnAnalisisInteligente.BackColor = BackColor;
            }
        }



        /// <summary>
        /// Método provicional para generar la Macros del Compilador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmiMacros_Click(object sender, EventArgs e)
        {
            foreach (var item in AnalizadorSintactico.Variables)
            {
                if (item.estado == -159)
                {
                    item.enUso = true;
                }
                item.enUso = true;
            }
            //TextWriter el_archivo = new StreamWriter(@"C:\Users\alex_\Documents\Git\compi-ii\Macros\COMP.asm");
            TextWriter el_archivo = new StreamWriter(@"C:\MASM\masm611\BIN\COMP.asm");

            ArrayList asmPrincipal = new ArrayList();
            ArrayList asm_medio = new ArrayList();
            ArrayList asm_final = new ArrayList();

            Stack<Token> p_valores = new Stack<Token>();
            Token op1, op2;
            string tempo_num = "tempo_num";
            string tempo_str = "tempo_str";
            string tempo_cmp = "tempo_cmp";


            #region 'Código inicial ensamblador'
            asmPrincipal.Add(" INCLUDE macros.mac");
            asmPrincipal.Add(" DOSSEG");
            asmPrincipal.Add("\n .MODEL SMALL");
            asmPrincipal.Add("\n .STACK 100h");
            asmPrincipal.Add("\n .DATA");
            asmPrincipal.Add("\n\t" + tempo_str + " db 254 dup('$')\t");
            asmPrincipal.Add("\n\t" + tempo_num + " \tDW \t?");
            asmPrincipal.Add("\n\t" + tempo_cmp + " \tDW \t?");
            asmPrincipal.Add("\n\tSalto db \" \", 10,13,'$'");
            asmPrincipal.Add("\n");
            #endregion
            #region 'Codigo Intermedio'

            asm_medio.Add("\n.CODE ");
            asm_medio.Add("\n.386 ");
            asm_medio.Add("\n BEGIN: ");
            asm_medio.Add("\n 		 MOV	AX, @DATA ");
            asm_medio.Add("\n		 MOV	DS, AX ");
            asm_medio.Add("\nCALL	COMP ");
            asm_medio.Add("\n		 MOV AX, 4C00H ");
            asm_medio.Add("\n	     INT 21H ");
            asm_medio.Add("\nCOMP PROC");
            #endregion
            #region 'Código final'
            asm_final.Add("\nret");
            asm_final.Add("\nCOMP endp");
            asm_final.Add("\nend BEGIN");
            #endregion


            //Agrega asmPrincipal a 'el_archivo'
            foreach (Object line in asmPrincipal)
            {
                el_archivo.Write(line);
            }

            //Agrega variables a 'el_archivo'
            foreach (Token token in AnalizadorSintactico.PolishCompleto)
            {
                if ((token.estado == -130 || token.estado == -120) && ChecarSiExiste(token))
                    el_archivo.Write("\n\t" + token.lexema + "\tDW \t?");
                else if (token.estado == -159 && ChecarSiExiste(token))
                    el_archivo.Write("\n\t" + token.lexema + " db 254 dup ('$')\t");
                else if (token.estado == -1)
                {
                    el_archivo.Write("\n\t" + "Temp" + AnalizadorSintactico.PolishCompleto.IndexOf(token) + " db \"" + token.lexema.Substring(1, token.lexema.Length - 2) + "$\"" + "\t");
                    token.lexema = "Temp" + AnalizadorSintactico.PolishCompleto.IndexOf(token);
                }
            }


            //Agrega asm_medio a 'el_archivo'
            foreach (object obj in asm_medio)
            {
                el_archivo.Write(obj);
            }
            foreach (Token nodo in AnalizadorSintactico.PolishCompleto)
            {
                if (nodo.estado == -1   ||nodo.estado == -2    || nodo.estado == -3 ||
                    nodo.estado == -159 || nodo.estado == -120 || nodo.estado == -130 || nodo.estado == -101)
                    p_valores.Push(nodo);
                else if (nodo.estado == -170)
                    el_archivo.Write("\n\t" + nodo.lexema);
                else if(nodo.estado == -180) //brI
                {
                    el_archivo.Write("\n\tJE " + nodo.lexema.Substring(4, 2));
                }
                else if (nodo.estado == -190) //brF
                {
                    el_archivo.Write("\n\tCMP " + tempo_cmp + ", 0");
                    el_archivo.Write("\n\tJE " + nodo.lexema.Substring(4,2));
                }
                else if(nodo.estado == -200)
                {
                    nodo.lexema = nodo.lexema.Substring(1);
                    if (nodo.lexema.Substring(0,1) == "\"")
                    {
                        nodo.lexema = nodo.lexema.Substring(0, nodo.lexema.Length - 1) + "$\"";
                    }
                    el_archivo.Write("\n\tWRITE " + nodo.lexema);
                    el_archivo.Write("\n\tWRITE " + "Salto");
                }
                else
                {
                    op2 = p_valores.Pop();
                    op1 = p_valores.Pop();
                    switch (nodo.estado)
                    {
                        case -16:
                            if (op1.estado == -1 || op2.estado == -1){
                                el_archivo.Write("\n\tCONCATENAR " + op1.lexema + ", " + op2.lexema + ", " + tempo_str);
                                op2.lexema = tempo_str;
                            }else{
                                el_archivo.Write("\n\tSUMAR " + op1.lexema + ", " + op2.lexema + ", " + tempo_num);
                                op2.lexema = tempo_num;
                            }
                            break;
                        case -18:
                            el_archivo.Write("\n\tRESTA " + op1.lexema + ", " + op2.lexema + ", " + tempo_num);
                            op2.lexema = tempo_num;
                            break;
                        case -17:
                            el_archivo.Write("\n\tMULTI " + op1.lexema + ", " + op2.lexema + ", " + tempo_num);
                            op2.lexema = tempo_num;
                            break;
                        case -19:
                            el_archivo.Write("\n\tDIVIDE " + op1.lexema + ", " + op2.lexema + ", " + tempo_num);
                            op2.lexema = tempo_num;
                            break;


                        case -21:
                            el_archivo.Write("\n\tI_MAYOR " + op1.lexema + ", " + op2.lexema + ", " + tempo_cmp);
                            op2.lexema = tempo_cmp;
                            break;
                        case -22:
                            el_archivo.Write("\n\tI_MENOR " + op1.lexema + ", " + op2.lexema + ", " + tempo_cmp);
                            op2.lexema = tempo_cmp;
                            break;
                        case -23:
                            el_archivo.Write("\n\tI_MAYORIGUAL " + op1.lexema + ", " + op2.lexema + ", " + tempo_cmp);
                            op2.lexema = tempo_cmp;
                            break;
                        case -24:
                            el_archivo.Write("\n\tI_MENORIGUAL " + op1.lexema + ", " + op2.lexema + ", " + tempo_cmp);
                            op2.lexema = tempo_cmp;
                            break;
                        case -25:
                            el_archivo.Write("\n\tI_IGUAL " + op1.lexema + ", " + op2.lexema + ", " + tempo_cmp);
                            op2.lexema = tempo_cmp;
                            break;
                        case -27:
                            if (op1.estado == -1 || op2.estado == -1)
                                el_archivo.Write("\n\tS_ASIGNAR " + op1.lexema + ", " + op2.lexema);
                            else
                                el_archivo.Write("\n\tI_ASIGNAR " + op1.lexema + ", " + op2.lexema);
                            break;
                        default: break;
                    }
                    p_valores.Push(op2);
                }
            }

            //Agrega asm_final a 'el_archivo'
            foreach (object obj in asm_final)
            {
                el_archivo.Write(obj);
            }

            el_archivo.Close();

        }
        /// <summary>
        /// Verifica que las variables existan
        /// </summary>
        /// <param name="token">Token a verificiar</param>
        /// <returns>Regresa Verdadero si la variable no existe, retorna Falso en caso de lo contrario</returns>
        private bool ChecarSiExiste(Token token)
        {
            foreach (var item in AnalizadorSintactico.Variables)
            {
                if (item.nombre == token.lexema && item.enUso == true)
                {
                    item.enUso = false;
                    return true;
                }
            }
            return false;
        }

        private void btnMacro_Click(object sender, EventArgs e)
        {
            tsmiMacros_Click(this, e);
        }
    }
}
