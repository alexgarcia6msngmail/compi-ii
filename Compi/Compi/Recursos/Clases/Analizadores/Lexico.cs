﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compi.Recursos.Clases.Analizadores
{
    public class Lexico
    {
        public List<Token> ListaTokens;
        
        public int[,] MT = new int[,]
        {/*     0       1       2       3       4      5      6      7      8      9      10     11     12     13     14     15     16     17     18     19     20     21     22     23     24     25    26     27      28      29   |*/
        /*  ||  "  ||  0-9  ||  .   ||  _  ||  A-Z  || '  ||  ,  ||  ;  ||  {  ||  }  ||  (  ||  )  ||  [  ||  ]  ||  :  ||  +  ||  *  ||  -  ||  /  ||  =  ||  %  ||  >  ||  <  ||  !  ||  |  ||  &  ||Intro|| LEOF ||  ♭  ||  OC ||*/
       /*   ,       ,       ,       ,       ,       ,     ,      ,      ,      ,      ,       ,      ,     ,       ,      ,     ,       ,      ,      ,      ,     ,      ,      ,      ,      ,      ,       ,      ,       ,       ,*/
/* 0 */     {   1   ,   2   ,   5   ,   6   ,   7   ,  8  ,  -6  ,  -7  ,  -8  ,  -9  ,  -10  , -11  , -12 , -13   , -15  , 13  ,   14  ,  15  , 16   ,  20  ,  17 ,  18  ,  19  ,  21  ,  22  ,  23  ,  0    ,  0    ,   0  ,  -506  },
/* 1 */     {  -1   ,   1   ,   1   ,   1   ,   1   ,  1  ,   1  ,   1  ,   1  ,   1  ,   1   ,  1   ,  1  ,   1   ,   1  ,  1  ,   1   ,  1   ,  1   ,  1   ,  1  ,  1   ,  1   ,  1   ,  1   ,  1   ,  1    , -500  ,   1  ,    1   },
/* 2 */     {  24   ,   2   ,   3   ,  24   ,  24   , 24  ,  -2  ,  -2  ,  24  ,  -2  ,  24   , -2   , 24  ,  -2   ,  -2  , -2  ,  -2   , -2   , -2   , -2   , -2  , -2   , -2   , -2   , -2   , -2   , -2    , -2    ,  -2  ,   24   },
/* 3 */     {  25   ,   4   ,  25   ,  25   ,  25   , 25  , -502 , -502 , -502 , -502 , -502  , -502 ,-502 , -502  , -502 ,-502 , -502  , -502 ,-502  ,-502  ,-502 , -502 , -502 , -502 , -502 , -502 , -502  , -502  , -502 ,   25   },
/* 4 */     {  25   ,   4   ,  25   ,  25   ,  25   , 25  ,  -3  ,  -3  ,  25  ,  -3  ,  25   ,  -3  , 25  ,  -3   ,  -3  , -3  ,  -3   ,  -3  , -3   , -3   , -3  ,  -3  ,  -3  , -3   , -3   , -3   , -3    , -3    ,  -3  ,   25   },
/* 5 */     { -14   ,   4   , -14   , -14   , -14   ,-14  , -14  , -14  , -14  , -14  , -14   , -14  , -14 , -14   , -14  , -14 , -14   , -14  , -14  , -14  , -14 , -14  , -14  , -14  , -14  , -14  , -14   , -14   ,  -14 ,   -14  },
/* 6 */     { -503  ,   7   , -503  ,   6   ,   7   ,-503 , -503 , -503 , -503 , -503 , -503  , -503 ,-503 , -503  , -503 ,-503 , -503  , -503 , -503 , -503 ,-503 , -503 , -503 , -503 , -503 , -503 , -503  , -503  , -503 ,  -503  },
/* 7 */     { -503  ,   7   ,  -4   ,   6   ,   7   ,-503 ,  -4  ,  -4  ,  -4  ,  -4  ,  -4   ,  -4  , -4  ,  -4   ,  -4  , -4  ,  -4   ,  -4  , -4   , -4   , -4  , -4   ,  -4  ,  -4  , -4   , -503 , -4    , -4    ,  -4  ,  -503  },
/* 8 */     {   9   ,   9   ,   9   ,   9   ,   9   ,-504 ,   9  ,   9  ,   9  ,   9  ,   9   ,   9  ,  9  ,  9    ,   9  ,   9 ,   9   ,   9  ,   9  ,   9  ,  9  ,  9   ,   9  ,  9   ,   9  ,  9   ,  9    , -504  ,   9  ,    9   },
/* 9 */     { -505  , -505  , -505  , -505  , -505  , -5  , -505 , -505 , -505 ,-505  , -505  , -505 ,-505 , -505  , -505 , -505, -505  , -505 , -505 , -505 , -505, -505 , -505 , -505 , -505 , -505 , -505  , -505  , -505 ,  -505  },
/*10 */     {  10   ,  10   ,  10   ,  10   ,  10   , 10  ,  10  ,  10  ,  10  ,  10  ,  10   ,  10  ,  10 ,  10   ,  10  ,  10 ,   11  ,  10  ,  10  ,  10  ,  10 ,  10  ,  10  ,  10  ,  10  ,  10  ,  10   ,  -507 ,  10  ,   10   },
/*11 */     {  10   ,  10   ,  10   ,  10   ,  10   , 10  ,  10  ,  10  ,  10  ,  10  ,  10   ,  10  ,  10 ,  10   ,  10  ,  10 ,   10  ,  10  ,  0   ,  10  ,  10 ,  10  ,  10  ,  10  ,  10  ,  10  ,  10   ,  -507 ,  11  ,   10   },
/*12 */     {  12   ,  12   ,  12   ,  12   ,  12   , 12  ,  12  ,  12  ,  12  ,  12  ,  12   ,  12  ,  12 ,  12   ,  12  ,  12 ,   12  ,  12  ,  12  ,  12  ,  12 ,  12  ,  12  ,  12  ,  12  ,  12  ,  0    ,  0    ,  12  ,   12   }, 
/*13 */     { -16   , -16   , -16   , -16   , -16   ,-16  , -16  , -16  , -16  , -16  , -16   , -16  , -16 , -16   ,  -16 , -39 ,  -16  , -16  , -16  , -41  , -16 , -16  , -16  , -16  , -16  , -16  , -16   , -16   , -16  ,  -16   },
/*14 */     { -17   , -17   , -17   , -17   , -17   ,-17  , -17  , -17  , -17  , -17  , -17   , -17  , -17 , -17   ,  -17 , -17 ,  -17  , -17  , -17  , -42  , -17 , -17  , -17  , -17  , -17  , -17  , -17   , -17   , -17  ,  -17   },
/*15 */     { -18   , -18   , -18   , -18   , -18   ,-18  , -18  , -18  , -18  , -18  , -18   , -18  , -18 , -18   ,  -18 , -18 ,  -18  , -40  , -18  , -43  , -18 , -18  , -18  , -18  , -18  , -18  , -18   , -18   , -18  ,  -18   },
/*16 */     { -19   , -19   , -19   , -19   , -19   ,-19  , -19  , -19  , -19  , -19  , -19   , -19  , -19 , -19   ,  -19 , -19 ,   10  , -19  ,  12  , -44  , -19 , -19  , -19  , -19  , -19  , -19  , -19   , -19   , -19  ,  -19   },
/*17 */     { -20   , -20   , -20   , -20   , -20   ,-20  , -20  , -20  , -20  , -20  , -20   , -20  , -20 , -20   ,  -20 , -20 ,  -20  , -20  , -20  , -45  , -20 , -20  , -20  , -20  , -20  , -20  , -20   , -20   , -20  ,  -20   },
/*18 */     { -21   , -21   , -21   , -21   , -21   ,-21  , -21  , -21  , -21  , -21  , -21   , -21  , -21 , -21   ,  -21 , -21 ,  -21  , -21  , -21  , -23  , -21 , -21  , -21  , -21  , -21  , -21  , -21   , -21   , -21  ,  -21   },
/*19 */     { -22   , -22   , -22   , -22   , -22   ,-22  , -22  , -22  , -22  , -22  , -22   , -22  , -22 , -22   ,  -22 , -22 ,  -22  , -22  , -22  , -24  , -22 , -22  , -22  , -22  , -22  , -22  , -22   , -22   , -22  ,  -22   },
/*20 */     { -27   , -27   , -27   , -27   , -27   ,-27  , -27  , -27  , -27  , -27  , -27   , -27  , -27 , -27   ,  -27 , -27 ,  -27  , -27  , -27  , -25  , -27 , -27  , -27  , -27  , -27  , -27  , -27   , -27   , -27  ,  -27   },
/*21 */     { -506  , -506  , -506  , -506  , -506  ,-506 , -506 , -506 , -506 , -506 , -506  , -506 , -506, -506  , -506 , -506, -506  , -506 , -506 , -26  , -506, -506 , -506 , -506 , -506 , -506 , -506  , -506  , -506 ,  -506  },
/*22 */     { -506  , -506  , -506  , -506  , -506  ,-506 , -506 , -506 , -506 , -506 , -506  , -506 , -506, -506  , -506 , -506,  -506 , -506 , -506 , -506 , -506, -506 , -506 , -506 , -29  , -506 , -506  , -506  , -506 ,  -506  },
/*23 */     { -506  , -506  , -506  , -506  , -506  ,-506 , -506 , -506 , -506 , -506 , -506  , -506 , -506, -506  , -506 , -506,  -506 , -506 , -506 , -506 , -506, -506 , -506 , -506 , -506 , -31  , -506  , -506  , -506 ,  -506  },

         /*     0       1       2       3       4      5      6      7      8      9      10     11     12     13     14     15     16     17     18     19     20     21     22     23     24     25    26     27      28      29   |*/
        /*  ||  "  ||  0-9  ||  .   ||  _  ||  A-Z  || '  ||  ,  ||  ;  ||  {  ||  }  ||  (  ||  )  ||  [  ||  ]  ||  :  ||  +  ||  *  ||  -  ||  /  ||  =  ||  %  ||  >  ||  <  ||  !  ||  |  ||  &  ||Intro|| LEOF ||  ♭  ||  OC ||*/
       /*   ,       ,       ,       ,       ,       ,     ,      ,      ,      ,      ,       ,      ,     ,       ,      ,     ,       ,      ,      ,      ,     ,      ,      ,      ,      ,      ,       ,      ,       ,       ,*/
/*24 */     {  24   ,  24   ,  24   ,  24   ,  24   , 24  , -501 , -501 , -501 , -501 , -501  , -501 ,-501 , -501  , -501 , -501,  -501 , -501 , -501 , -501 , -501, -501 , -501 , -501 , -501 , -501 , -501  , -501  , -501 ,   24   },
/*25 */     {  25   ,  25   ,  25   ,  25   ,  25   , 25  , -502 , -502 , -502 , -502 , -502  , -502 ,-502 , -502  , -502 ,-502 , -502  , -502 ,-502  ,-502  ,-502 , -502 , -502 , -502 , -502 , -502 , -502  , -502  , -502 ,   25   },
/*26 */
        };
        
        public string[,] PalabrasReservadas = new string[,]
        {
            {"bool", "-101"},       {"break", "-102" },     {"byte", "-103"},    {"case","-105"},
            {"catch","-106"},       {"char","-107"},        {"class","-108"},    
            {"default","-112"},     {"double","-114"},      {"else","-115"},     {"extends","-116"},  {"false","-117"},
            {"finally","-119"},     {"float","-120"},       {"for","-121"},
            {"if","-125"},          {"import","-128"},
            {"int","-130"},         {"long","-132"},        {"new","-134"},    
            {"private","-139"},     {"protected","-140"},   {"public","-141"},
            {"return","-143"},      {"short","-144"},       {"switch","-147"},
            {"true","-154"},        {"try","-155"},         {"var","-156"},      {"void","-157"},     {"while","-158"},  
            {"string","-159"},      {"Scanner", "-160"},    {"print", "-161" }
        };

        public string[,] Errores = new string[,]
        {
            {"Formato de cadena invalido",         "-500"},
            {"Formato numerico invalido",          "-501"},
            {"Formato decimal no valido",          "-502"},
            {"Formato de Identificador no valido", "-503"},
            {"Formato de caracter no valido",      "-504"},
            {"Simbolo no valido",                  "-505"},
            {"Simbolo desconocido",                "-506"},
            {"Comentario incompleto",              "-507"},
            {"Error Sintáctico, Se esperaba un :", "-550"},
        };

        //Estados en los cuales uno llegó utilizando Delimitadores de Palabras
        int[] DelimitadorPalabra = new int[]
        {-2,-3, -4,-5,-14,-16, -17, -18, -19, -20, -21, -22, -27, 28, -30, -32, -35, -36, -37, -38,
        -501 , -502};

        char SiguienteCaracter(string Texto, int index)
        {
            return Convert.ToChar(Texto.Substring(index, 1));
        }

        bool SeUsóUnDelimitadorDePalabra(int estado)
        {
            for (int i = 0; i < DelimitadorPalabra.Length; i++)
            {
                if (estado == DelimitadorPalabra[i])
                {
                    return true;
                }
            }
            return false;
        }
        public int linea;
        public void Ejecutar(string Texto)
        {
            linea = 1;
            ListaTokens = new List<Token>();
            
            int estado = 0; //fila
            int columna = 0;
            string lexema = "";
            char caracter;

            for (int i = 0; i < Texto.Length; i++)
            {

                #region Busqueda de Columna y Estado
                caracter = SiguienteCaracter(Texto, i);
                if (char.IsDigit(caracter))
                {
                    columna = 1;
                }
                else if (char.IsLetter(caracter))
                {
                    columna = 4;
                }
                else
                {

                    switch (caracter)
                    {
                        case '"': columna = 0;  break;
                        case '.': columna = 2;  break;
                        case '_': columna = 3;  break;
                        case '\'':columna = 5;  break;
                        case ',': columna = 6;  break;
                        case ';': columna = 7;  break;
                        case '{': columna = 8;  break;
                        case '}': columna = 9;  break;
                        case '(': columna = 10; break;
                        case ')': columna = 11; break;
                        case '[': columna = 12; break;
                        case ']': columna = 13; break;
                        case ':': columna = 14; break;
                        case '+': columna = 15; break;
                        case '*': columna = 16; break;
                        case '-': columna = 17; break;
                        case '/': columna = 18; break;
                        case '=': columna = 19; break;
                        case '%': columna = 20; break;
                        case '>': columna = 21; break;
                        case '<': columna = 22; break;
                        case '!': columna = 23; break;
                        case '|': columna = 24; break;
                        case '&': columna = 25; break;
                        case '\n':columna = 26;
                            linea ++;           break;       // Intro
                        case '\r':columna  = 26;break;
                        case '\t':columna  = 26;break;
                        case (char)165://LEOF  (SIMBOLO UTILIZADO PARA RECONOCER EL FINAL DEL MENSAJE) ¥
                                  columna = 27; break; 
                        case ' ': columna = 28; break;       // Space
                        default : columna = 29; break;       // Otra Cosa
                    }
                }
                estado = MT[estado, columna];
                #endregion

                lexema += caracter;

                if (estado == 0)
                {
                    lexema = "";
                }
                if (estado < 0)
                {
                    if (SeUsóUnDelimitadorDePalabra(estado))
                    {
                        lexema = lexema.Substring(0, lexema.Length - 1);
                        i--;
                    }
                    if (estado <= -500)
                    {
                        // Si entramos aqui es porque encontramos un error
                        // Por lo que enviaremos dicho error en nuestra lista de tokens
                        for (int k = 0; k < Errores.GetLength(0); k++)
                        {
                            if (estado == Convert.ToInt32(Errores[k,1]))
                            {
                                lexema = Errores[k, 0];
                            }
                        }
                    }
                    else if (estado == -4)
                    {
                        // Aqui revisamos si el identificador es en realidad un 
                        //identificador y si no es una palabra reservada
                        for (int j = 0; j < PalabrasReservadas.GetLength(0); j++)
                        {
                            if (lexema == PalabrasReservadas[j,0])
                            {
                                estado = Convert.ToInt32(PalabrasReservadas[j,1]);
                            }
                        }
                    }

                    ListaTokens.Add(new Token(estado, linea, lexema));
                    lexema = "";
                    estado = 0;
                }
            }
        }
    }

    public class Token
    {
        public int estado;
        public int linea;
        public string lexema;
        public int priority;

        public Token(int estado, int linea, string lexema)
        {
            this.estado = estado;
            this.lexema = lexema;
            this.linea = linea;
        }
    }
}
