﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compi.Recursos.Clases.Interfaz
{
    class LogicaInterfaz
    {
        /// <summary>
        /// Método utilizado para cambiar el contenido del archivo demostrado en pantalla dependiendo de
        /// la pestaña que se encuentra en uso
        /// </summary>
        /// <param name="ListaPlantillas">Lista de plantillas de
        /// las pestañas que se estan utilizando actualmente</param>
        /// <param name="CabeceraActual">Nombre del archivo que se esta utilizando</param>
        /// <returns></returns>
        public string ActualizarTextbox(List<Plantilla> ListaPlantillas, string CabeceraActual)
        {
            foreach (var plantilla in ListaPlantillas)
            {
                if (plantilla.Cabecera == CabeceraActual)
                {
                    return plantilla.Contenido;
                }
            }
            return "Exepción no manejada";
        }
    }
}
