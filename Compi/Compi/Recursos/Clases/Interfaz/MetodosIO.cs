﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compi.Recursos.Clases.Interfaz
{
    class MetodosIO
    {
        /// <summary>
        /// Metodo utilizado para abrir archivos e implementarlos en el proyecto Compi
        /// </summary>
        /// <param name="ListaPlantillas">Lista de plantillas utilizadas para manejarse alrededor de
        /// las pestañas del TabControl de la interfaz</param>
        /// <param name="ruta">Ruta recibida por el OpeFileDialog.FileName, la ruta donde conseguimos
        /// el archivo que deseamos abrir</param>
        /// <returns></returns>
        public Plantilla AbrirArchivo(List<Plantilla> ListaPlantillas, string ruta)
        {
            using (StreamReader sr = new StreamReader(ruta))
            {
                foreach (var plantilla in ListaPlantillas)
                {
                    if (plantilla.Cabecera == Path.GetFileName(ruta))
                    {
                        return null;
                    }
                }
                return  new Plantilla(Path.GetFileName(ruta),
                    sr.ReadToEnd(), ruta);
            }
        }
    }
}
