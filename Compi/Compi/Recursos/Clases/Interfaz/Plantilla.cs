﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compi.Recursos.Clases.Interfaz
{
    class Plantilla
    {
        string cabecera;
        string ruta;
        string contenido;

        /// <summary>
        /// Constructor para generar una plantilla en la que se almacenará la información
        /// de las plantillas que se estan utilizando
        /// </summary>
        /// <param name="cabecera">Nombre de la cadena que contendrá la cabecera del TabPage correspondiente</param>
        /// <param name="contenido">Contenido del archivo con el que se esta trabajando</param>
        /// <param name="ruta">Nombre de la ruta de acceso del archivo</param>
        public Plantilla(string cabecera, string contenido, string ruta)
        {
            this.cabecera = cabecera;
            this.contenido = contenido;
            this.ruta = ruta;
        }

        public string Cabecera
        {
            get { return cabecera; }
            set { cabecera = value; }
        }

        public string Contenido
        {
            get { return contenido; }
            set { contenido = value; }
        }

        public string Ruta
        {
            get { return ruta; }
            set { ruta = value;}
        }
    }
}
